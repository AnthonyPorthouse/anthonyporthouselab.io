import Vue from 'vue';

import App from './components/App.vue';

import './scss/app.scss';

/* eslint no-new: off */
new Vue({
  el: '#app',
  components: {
    app: App,
  },
});
