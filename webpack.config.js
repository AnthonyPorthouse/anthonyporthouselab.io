const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

let plugins = [
  new CleanWebpackPlugin(['./static', './themes/*/static']),
  new ExtractTextPlugin('css/[name].[hash].css'),
];

const defaults = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: [
            'css-loader',
          ],
        }),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: [
            'css-loader',
            'sass-loader',
          ],
        }),
      },
      {
        test: /\.sass$/,
        use: ExtractTextPlugin.extract({
          fallback: 'vue-style-loader',
          use: [
            'css-loader',
            'sass-loader?indentedSyntax',
          ],
        }),
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            scss: ExtractTextPlugin.extract({
              fallback: 'vue-style-loader',
              use: [
                'css-loader',
                'sass-loader',
              ],
            }),
            sass: ExtractTextPlugin.extract({
              fallback: 'vue-style-loader',
              use: [
                'css-loader',
                'sass-loader?indentedSyntax',
              ],
            }),
          },
          // other vue-loader options go here
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /(^fonts\/)\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        },
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]?[hash]',
        },
      },
    ],
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js',
    },
    extensions: ['*', '.js', '.vue', '.json'],
  },
  performance: {
    hints: false,
  },
  devtool: '#eval-source-map',
  plugins: [
    new CleanWebpackPlugin(['./static', './themes/*/static']),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendor.[hash].js',
      minChunks: Infinity,
    }),
    new ExtractTextPlugin('css/[name].[hash].css'),
    new ManifestPlugin({
      fileName: '../data/manifest.json',
    }),
  ],
};

if (process.env.NODE_ENV === 'production') {
  defaults.devtool = '#source-map';
  // http://vue-loader.vuejs.org/en/workflow/production.html
  plugins = (plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false,
      },
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
  ]);
}

module.exports = [
  // Frontend
  Object.assign({}, defaults, {
    entry: {
      main: './assets/main.js',
    },
    output: {
      path: path.resolve(__dirname, './static'),
      publicPath: '/',
      filename: 'js/[name].[hash].js',
    },
    plugins: plugins.concat([
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'js/vendor.[hash].js',
        minChunks: Infinity,
      }),
      new ManifestPlugin({
        fileName: '../data/manifest.json',
      }),
    ]),
  }),

  // Theme
  Object.assign({}, defaults, {
    entry: {
      theme: './themes/porthou.se/assets/theme.js',
    },
    output: {
      path: path.resolve(__dirname, './themes/porthou.se/static'),
      publicPath: '/',
      filename: 'js/[name].[hash].js',
    },
    plugins: plugins.concat([
      new webpack.optimize.CommonsChunkPlugin({
        name: 'themeVendor',
        filename: 'js/themeVendor.[hash].js',
        minChunks: Infinity,
      }),
      new ManifestPlugin({
        fileName: '../data/themeManifest.json',
      }),
    ]),
  }),
];